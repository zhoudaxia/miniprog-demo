const gulp = require('gulp')
const less = require('gulp-less')
const del = require('del')
const rename = require('gulp-rename')
const eslint = require('gulp-eslint')
const babel = require('gulp-babel')
const base64 = require('gulp-base64')

const styleExts = ['.wxss', 'less']
const srcPath = 'src/**/*'
const stylePath = styleExts.map(_ => srcPath + _)
const jsPath = srcPath + '.js'
const wxmlPath = srcPath + '.wxml'
const jsonPath = srcPath + '.json'
const imgExts = ['.png', '.jpg']
const imgPath = imgExts.map(_ => srcPath + _)

const distPath = 'dist/'

const tasks = {
  wxss: {
    path: stylePath,
    func () {
      return gulp
        .src('src/**/*.wxss', {since: gulp.lastRun('wxss')})
        .pipe(less())
        .pipe(base64({
          baseDir: './src/assets'
        }))
        .pipe(
          rename(function (path) {
            path.extname = '.wxss'
          })
        )
        .pipe(gulp.dest(distPath))
    }
  },
  wxml: {
    path: wxmlPath,
    func () {
      return gulp
        .src(wxmlPath, {since: gulp.lastRun('wxml')})
        .pipe(gulp.dest(distPath))
    }
  },
  js: {
    path: jsPath,
    func () {
      return gulp
        .src(jsPath, {since: gulp.lastRun('js')})
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(babel())
        .pipe(gulp.dest(distPath))
    }
  },
  json: {
    path: jsonPath,
    func () {
      return gulp
        .src(jsonPath, {since: gulp.lastRun('json')})
        .pipe(gulp.dest(distPath))
    }
  },
  img: {
    path: imgPath,
    func () {
      return gulp
        .src(imgPath, { since: gulp.lastRun('img')})
        .pipe(gulp.dest(distPath))
    }
  }
}

gulp.task('watch', () => {
  for (let [name, task] of Object.entries(tasks)) {
    gulp.watch(task.path, gulp.series(name))
  }
})

for (let [name, task] of Object.entries(tasks)) {
  gulp.task(name, task.func)
}

gulp.task('clean', done => {
  del.sync(['dist/**/*'])
  setTimeout(() => {
    done()
  }, 500)
})

gulp.task('dev', gulp.series('clean', gulp.parallel('wxml', 'js', 'json', 'wxss', 'img'), 'watch'))
gulp.task('build', gulp.series('clean', gulp.parallel('wxml', 'js', 'json', 'wxss', 'img')))
