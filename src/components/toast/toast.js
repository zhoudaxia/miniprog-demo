Component({
  properties: {
    type: String
  },
  externalClasses: ['my-class'],
  options: {
    addGlobalClass: false
  },
  data: {
    text: 'success!'
  },
  methods: {

  }
})
