Component({
  properties: {
    a: {
      type: String
    }
  },
  data: {
    firstName: 'mao'
  },
  methods: {
    change () {
      this.setData({firstName: 'zhou'})
    },
    jump () {
      wx.navigateTo({url: '/pages/index/index'})
    },
    onLoad () {
      console.log('abc')
    }
  }
})
