//index.js
const regeneratorRuntime = require('/lib/regenerator-runtime')
import Test from '/service/models/Test'

let todoId = 0
Component({
  data: {
    todos: [
      {text: 'abc', done: false, id: todoId++},
      {text: 'hall', done: true, id: todoId++},
      {text: 'haha', done: true, id: todoId++}
    ],
    inputValue: '',
    focus: true,
    filterClass: ''
  },
  async attached () {
    let a = await Test.hello()
    console.log('aaaaa', a)
  },
  methods: {
    toggle ({currentTarget: {dataset}}) {
      let index = dataset.index
      let todos = this.data.todos
      todos[index].done = !todos[index].done
      this.setData({todos})
    },
    add ({detail}) {
      let todos = this.data.todos
      todos.push({
        id: todoId++,
        text: detail.value,
        done: false
      })
      this.setData({todos, inputValue: '', focus: true})
    },
    remove ({currentTarget: {dataset}}) {
      let todos = this.data.todos
      todos.splice(dataset.index, 1)
      this.setData({todos, focus: true})
    },
    filter ({currentTarget: {dataset}}) {
      this.setData({filterClass: dataset.filter})
    },
    clear () {
      let todos = this.data.todos
      todos = todos.filter(todo => {
        return !todo.done
      })
      this.setData({todos})
    }
  }
})
