module.exports = function (opts) {
  return new Promise(resolve => {
    opts.success = data => {
      resolve(data.data)
    }
    opts.fail = data => {
      console.log(data)
    }
    wx.request(opts)
  })
}
