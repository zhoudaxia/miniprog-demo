import request from '../index'
import {BaseUrl} from '@/decorator.js'

@BaseUrl('http://localhost:3001')
class Base {
  static $name = ''
  get (url, data, opts = {}) {
    opts.method = 'get'
    return this._request(url, data, opts)
  }
  post (url, data, opts) {
    opts.method = 'post'
    return this._request(url, data, opts)
  }
  _request (url, data, opts) {
    url = `${Base.$name}/${this.constructor.$name}/${url}`
    opts = Object.assign({url, data}, opts)
    return request(opts)
  }
}
export default Base
