let regeneratorRuntime = require('/lib/regenerator-runtime.js')
import {BaseUrl} from '@/decorator.js'
import Base from './Base'

@BaseUrl('test')
class Test extends Base {
  async hello () {
    let data = await this.get('hello')
    return data
  }
}

export default new Test()
