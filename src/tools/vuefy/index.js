const observe = require('./observe')
const Watcher = require('./watcher')

module.exports = Behavior({
  lifetimes: {
    attached () {
      this._vuefy()
      this._originSetData = this.setData
      this.setData = data => {
        this._originSetData(data)
        this._afterSetData(data)
      }
    }
  },
  definitionFilter (opts) {
    let {computed, watch} = opts
    opts.methods._vuefy = function () {
      observe(this.data)
      for (let key in computed) {
        let updateFunc = () => {
          this.setData({[key]: computed[key].call(this.data)})
        }
        new Watcher(updateFunc)
        updateFunc()
      }
      this._afterSetData = function (data) {
        for (let key in data) {
          if (watch[key]) {
            watch[key].call(this.data, data[key])
          }
        }
      }
    }
  }
})
