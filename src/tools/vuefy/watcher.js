module.exports = function Watcher (update) {
  /**
   * 监视器，通过get添加到dep
   *
   * @param {Function} update
   */

  this.update = v => {
    update(v)
    Watcher.tmp = null
    this.update = update
  }
  Watcher.tmp = this
  //Watcher.tmp = null
}
